<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Facture;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture 
{
    private $passwordEncoder;

    public const ADMIN_USER_REFERENCE = 'admin-user';

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
       

        $user = new User();
        $user->setUsername('Walid')
            ->setPlainPassword('test')
            ->setPassword( $this->passwordEncoder->encodePassword($user, $user->getPlainPassword()))
            ->setRoles(["ROLE_ADMIN"]); 

        $manager
            ->persist($user);

        $manager->flush();
        for($i=1; $i<=150; $i++) {
            $prixHT = 100+10*$i;
            $prixTTC = 100+10*$i+((100+10*$i)*20)/100;
            $facture = new Facture();
            $facture->setDesignation('Designation'.$i)
                 ->setDescription('Designation'.$i)
                 ->setprixHT(floatval($prixHT))
                 ->setPrixTTC(floatval($prixTTC));
                 $manager
                 ->persist($facture);
     
             $manager->flush();       
        }
        

        $this->addReference(self::ADMIN_USER_REFERENCE, $user);
    }

    public function getDependencies()
    {
        return array(
            ServiceFixtures::class,
        );
    }
}