<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Facture;
use App\Form\FactureType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\DataTableManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DashboardController extends AbstractController
{
 // TODO suprimer Dashboard link et garanted
    /**
     * @Route("/", name="dashboard")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $facture = new Facture();
        $form = $this->createForm(FactureType::class, $facture);
        $form->handleRequest($request);
         // dd();
         //$request->isXmlHttpRequest() &&
         
        if (!$request->isXmlHttpRequest() && $request->getMethod() == "POST") {
            
            if ($form->isSubmitted() && $form->isValid()) {
               $object = $form->getData();
               if($object->getId() !=null) {
                $facture = $em->getRepository(Facture::class)->findOneBy(['id'=> $object->getId()]);
                $facture->setDesignation($object->getDesignation());
                $facture->setDescription($object->getDescription());
                $facture->setPrixHT($object->getPrixHT());
                $facture->setPrixTTC($object->getPrixHT());
                $em->persist($facture);
                $em->flush();
               } else {
                $object = $form->getData();
                if($object instanceof Facture)  {
                    $em->persist($object);
                    $em->flush();
                }  
                
               }    
                $this->addFlash('success', 'Article Modifier');
            } 
    }
       
       
        $paginations =$em->getRepository(Facture::class)->findAll();
        return $this->render('dashboard/index.html.twig',['paginations'=>$paginations,   
             'factureForm' => $form->createView(),]);
    }
    
    
    /**
     * @Route("/getfacture",  name="getfacture",  options={"expose"=true})
     */
    
    public function getFacture(Request $request , EntityManagerInterface $em)
    {
       if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Error'),
            400);
        } 

        $id= $request->request->get('id');
     
        $facture = $em->getRepository(Facture::class)->findOneBy(['id'=>$id]);  

        $factureArray=['id'=>$id, 'designation'=> $facture->getDesignation(), 
        'description'=> $facture->getDescription() ,'prixHT'=> $facture->getPrixHT(), 'prixTTC'=> $facture->getPrixTTC()];
        
        return new JsonResponse( $factureArray);
        
    }

    /**
     * @Route("/deletefacture",  name="delete-facture",  options={"expose"=true})
     */
    
    public function deleteFacture(Request $request , EntityManagerInterface $em)
    {
       if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Error'),
            400);
        } 

        $id= $request->request->get('id');
     
        $facture = $em->getRepository(Facture::class)->findOneBy(['id'=>$id]);  
        $em->remove($facture);
        $em->flush();
        return new Response(
            'Content',
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
        
    }



}