<?php

namespace App\Service;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Class DataTableManager
 *
 * @package App\Service
 */
class DataTableManager
{
    /**
     * @var SerializerBuilder $serializer
     */
    private $serializer;

    /**
     * ProjectManager constructor.
     */
    public function __construct()
    {
        $this->serializer = SerializerBuilder::create();
    }

    /**
     * @param Paginator $paginator
     * @return mixed
     */
    public function toJsonDataTable(Paginator $paginator)
    {
        $output = [
            'data' => $paginator->getQuery()->getResult(),
            'recordsFiltered' => $paginator->count(),
            'recordsTotal' => $paginator->count()
        ];
    
        $this->serializer->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        $this->serializer = $this->serializer->build();

        return $this->serializer->serialize($output, 'json');
    }
}