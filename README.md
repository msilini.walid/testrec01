# testrec01

1/ Créer une application symfony 5
Citer la ou les commandes utilisées pour la créatin du projet: 
1- installer composer 
2- composer create-project symfony/website-skeleton testrec01
3- configuration .env (host, user, password, dbname)
4- php bin/console doctrine:database:create

3/ Réaliser un module d’authentification permettant d’accéder à une page sécurisée (Vous pouvez utiliser un plugin pour cela) 
Argumenter le choix (ou pas) d’un plugin: 
FriendsOfSymfony/FOSUserBundle n'est plus maintenu
1- j’ai créer un entité User
2- php bin/console make:migration
php bin/console doctrine:migrations:migrate
3- créer un authentification par la commande :
php bin/console make:auth
4- liaison avec l’entité et paramétrage de redirection de contrôler en cas de succès.

5/ Créer des fixtures pour alimenter une centaine de factures.
1- création un fixture pour un utilisateur
2- création un loop(150) de fake de données pour alimenter la base par les factures
php bin/console doctrine:fixtures:load

6/ Ajouter une pagination dans la liste des factures avec un nombre de factures par défaut de  5 par page. Vous pouvez utiliser un plugin/bundle. Argumenter le choix (ou pas du plugin/bundle)
1- installation DataTable 
7/ Proposer des tests unitaires pour cette fonctionnalité.
1-  php bin/phpunit

8 / Proposer un mini guide d’installation de cette application et de lancement des tests
Guide  d’installation et de test de l’application :
1- install composer
2- git clone git@gitlab.com:msilini.walid/testrec01.git
3- composer install
4- php bin/console doctrine:database:create
5-php bin/console doctrine:migrations:migrate
6- php bin/console doctrine:fixtures:load
7-mettre le fichier testrec01_5458.conf dans l’alias de serveur et redémarrer le Serveur apache
9-tapper l’url :  http://www.testrec01.local:5458
10-login: walid et password: test
11- php bin/phpunit 
