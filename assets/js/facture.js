$(document).ready(function () {
    $('#table_facture').DataTable({ "pagingType": "full_numbers" });

    $('#btn-facture').on('submit', function (e) {
        console.log("walid");
        var id = $(this).attr('data-id');
        $.ajax({
            url: "{{ path('dashboard') }}",
            data: {
                'id': id
            },
            method: "POST",
            dataType: "json",
            success: function (response) {
                $('#facture_id').val("");
                $('#facture_designation').val("");
                $('#facture_description').val("");
                $('#facture_prixHT').val("");
                $('#facture_prixTTC').val("");
            }
        });

    });

    $('.edit-row').on('click', function (e) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "{{ path('getfacture') }}",
            data: {
                'id': id
            },
            method: "POST",

            success: function (response) {
                $('#facture_id').val(response.id);
                $('#facture_designation').val(response.designation);
                $('#facture_description').val(response.description);
                $('#facture_prixHT').val(response.prixHT);
                $('#facture_prixTTC').val(response.prixTTC);
            }
        });
    });

    $('.delete-row').on('click', function (e) {
        var id2 = $(this).attr('data-id');
        $.ajax({
            url: "{{ path('delete-facture') }}",
            data: {
                'id': id2
            },
            method: "POST",

            success: function (response) {
                window.history.forward(1)
                location.reload(true);
            }
        });
    });

    $('#add-row').on('click', function (e) {
        $('#facture_id').val("");
        $('#facture_designation').val("");
        $('#facture_description').val("");
        $('#facture_prixHT').val("");
        $('#facture_prixTTC').val("");
    });

});