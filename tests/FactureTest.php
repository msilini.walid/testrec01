<?php
namespace App\Tests\Entity;

use App\Entity\Facture;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class FactureTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Facture
    {
        return (new Facture())
        ->setDesignation('designation')
        ->setDescription('description')
        ->setPrixHT(20)
        ->setPrixTTC(50);
    }

    public function assertHasErrors(Facture $facture, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($facture);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(', ', $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }


    public function testInvalidBlankCodeEntity()
    {
        $this->assertHasErrors($this->getEntity()->setDesignation(''), 'test');
    }
}